#include <iostream> 
using namespace std;
int main() 
{
	int Jml_Hari[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
	int tanggal, bulan, tahun, pemain;
	//Baris pertama tanggal (tanggal, bulan, tahun)
	awal:
	cout << "==========================================================" << endl;
	//inputtanggal
	cout << "Masukkan tanggal : ";
	cin >> tanggal;
	//penentuantanggalharusesuai
	if (tanggal>31)
	{
		cout << "Tanggal tidak valid" << endl;
		goto awal;
	}
	//inputbulan
	cout << "Masukkan bulan : ";
	cin >> bulan;
	//penentuanbulanharusesuai
	if (bulan>12)
	{
		cout << "Bulan tidak valid" << endl;
		goto awal;
	}
	else if (tanggal>30 && (bulan==4 || bulan==6 || bulan==9 || bulan==11))
	{
		cout << "Tanggal tidak valid" << endl;
		goto awal;
	}

	//inputtahun
	cout << "Masukkan tahun : ";
	cin >> tahun;
	if (tahun<0)
	{
		cout << "Tahun tidak valid" << endl;
		goto awal;
	}
	//tahunbiasadibulanfebruari
	if (bulan==2 && (tahun%4!=0 && tanggal>28))
	{
		cout << "Tanggal tidak valid" << endl;
		goto awal;
	}
	else if (bulan == 2 && tahun%4!=0)
	{
		Jml_Hari[1]=28;
	}
	
	//penentuantahunkabisat
	if (bulan == 2 && (tahun%4==0 && tanggal>29))
	{
		cout << "Tanggal tidak valid" << endl;
		goto awal;
	}
	else if (bulan == 2 && tahun%4==0)
	{
		Jml_Hari[1]=29;
	}
	
	//baris kedua diisi jumlah pemain (2≤N≤10),
	cout << "Masukkan jumlah pemain (Antara 2-10 pemain) : ";
	cin >> pemain;
	int tebakan [pemain];
	cout << "==========================================================" << endl;
	//menjumlahkanharidariarray
	int jumlah=0;
	for (int i=0; i<bulan-1; i++)
	{
		jumlah=jumlah+Jml_Hari[i];
	}
	//formulatebakanhariyangbenar
	int jumlahhari=jumlah + tanggal;
	for (int i=0; i<pemain; i++)
	{
		cout << "Masukkan jumlah hari menurut pemain ke-" << i+1 << " : ";
		cin >> tebakan[i];
		if (tebakan[i]>jumlahhari)
		{
			tebakan[i]=0;
		}
	}
	int maks=0;
	for (int i=0; i<pemain; i++)
	{
		if (tebakan[i]>maks)
		{
			maks=tebakan[i];
		}
	}
	cout << "==========================================================" << endl;
	//caripemaindengantebakanpalingmendekati
	cout <<"Daftar pemain dengan tebakan yang benar/hampir benar :" << endl;
	for (int i=0; i<pemain; i++)
	{
		if (tebakan[i]<maks)
		{
			tebakan[i]=0;
			continue;
		}
		else if (tebakan[i]==0)
		{
			cout << "Tidak ada" << endl;
			break;
		}
		else if (tebakan [i] < jumlahhari && tebakan[i]!=0)
		{
			cout << "Pemain ke-" << i+1 << " dengan tebakan " << tebakan[i] << " hari (mendekati tebakan yang benar)" << endl;
		}
		else if (tebakan[i]==jumlahhari)
		{
			cout << "Pemain ke-" << i+1 << " dengan tebakan " << tebakan[i] << " hari (tebakan benar)" << endl;
		}
	}
	
	cout <<"Tebakan yang benar adalah " << jumlahhari << " hari" << endl;	
	
	return 0;
}
